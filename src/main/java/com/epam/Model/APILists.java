package com.epam.Model;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.epam.Application.*;

public class APILists {

    private final int limit = 10;
    private final int low = 0;
    private final int high = 100;

    public void getMin() {
        int[] integers = (new Random().ints(limit, low, high).toArray());
        for (int i = 0; i < integers.length; i++)
            logger.info("integers[" + i + "] = " + integers[i]);
        int min = Arrays.stream(integers).min().orElseThrow(NoSuchElementException::new);
        logger.info("Min = " + min);
    }

    public void getMax() {
        int[] integers = (new Random().ints(limit, low, high).toArray());
        for (int i = 0; i < integers.length; i++)
            logger.info("integers[" + i + "] = " + integers[i]);
        int max = Arrays.stream(integers).max().orElseThrow(NoSuchElementException::new);
        logger.info("Max = " + max);
    }

    public void getSum() {
        int[] integers = (new Random().ints(limit, low, high).toArray());
        for (int i = 0; i < integers.length; i++)
            logger.info("integers[" + i + "] = " + integers[i]);
        int sum = Arrays.stream(integers).sum();
        logger.info("Sum = " + sum);
        sum = Arrays.stream(integers).reduce(0, (a, b) -> a + b);
        logger.info("Sum using reduce = " + sum);
    }

    public void getAvrg() {
        int[] integers = (new Random().ints(limit, low, high).toArray());
        for (int i = 0; i < integers.length; i++)
            logger.info("integers[" + i + "] = " + integers[i]);
        int sum = Arrays.stream(integers).sum();
        long count = Arrays.stream(integers).count();
        long avrg = sum / count;
        logger.info("Average = " + avrg);
    }

    public void getAmountOfNumbersBiggerAvrg() {
        int[] integers = (new Random().ints(limit, low, high).toArray());
        for (int i = 0; i < integers.length; i++)
            logger.info("integers[" + i + "] = " + integers[i]);
        int sum = Arrays.stream(integers).sum();
        long count = Arrays.stream(integers).count();
        long avrg = sum / count;
        logger.info("Average = " + avrg);
        long amount = Arrays.stream(integers).filter(number -> number > avrg).count();
        logger.info("Amount of numbers bigger than average = " + amount);
    }

    public void createApplication() {
        Scanner type = new Scanner(System.in);
        boolean END = false;
        while (!END) {
            logger.info("Type some text.");
            String someText = type.nextLine();
            logger.info("You typed: " + someText);
            if (someText != null && !someText.equals("")) {
                String[] words = someText.split(" ");
                Stream<String> stream = Arrays.stream(words);
                long uniqueWords = stream.map(String::toLowerCase).distinct().count();
                Stream<String> stream2 = Arrays.stream(words);
                List<String> uniqueWordsList = stream2.map(String::toLowerCase).distinct().sorted().collect(Collectors.toList());
                logger.info("Number of unique words: " + uniqueWords);
                logger.info("Sorted list of unique words: " + uniqueWordsList.toString());

                Stream<String> stream3 = Arrays.stream(words);
                List<String> wordList = stream3.map(String::toLowerCase).sorted().collect(Collectors.toList());
                int[] value = new int[wordList.size()];
                for (int i = 0; i < wordList.size(); i++) {
                    value[i] = 1;
                }
                for (int i = 0; i < wordList.size() - 1; i++) {
                    for (int j = i + 1; j < wordList.size() - 1; j++) {
                        if (wordList.get(i).equals(wordList.get(j))) {
                            value[i] += 1;
                        }
                    }
                }
                wordList = wordList.stream().sorted().distinct().collect(Collectors.toList());
                for (int i = 0; i < wordList.size(); i++) {

                    logger.info("Word " + wordList.get(i) + " occurs " + value[i] + " times");
                }

            } else {
                END = true;
            }
        }
    }
}
