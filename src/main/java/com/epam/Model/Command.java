package com.epam.Model;

public interface Command {
    String execute(String command,String value);
}
