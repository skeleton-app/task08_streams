package com.epam.Model;

public class BusinessLogic {
    public int getMax(int a, int b, int c) {
        BiFunction firstLambda = (firstNum, secondNum, thirdNum) -> {
            return Math.max(firstNum, Math.max(secondNum, thirdNum));
        };
        return firstLambda.function(a, b, c);
    }

    public int getAverage(int a, int b, int c) {
        BiFunction secondLambda = (firstNum, secondNum, thirdNum) -> {
            return ((firstNum + secondNum + thirdNum) / 3);
        };
        return secondLambda.function(a, b, c);
    }


    Command operationCommand1 = (command, value) -> {
        return realizationCommand3(command, value);
    };
    Command operationCommand2 = new Command() {
        @Override
        public String execute(String command, String value) {
            return realizationCommand3(command, value);
        }
    };
    Command operationCommand3 = this::realizationCommand3;

    private String realizationCommand3(String command, String value) {
        if (command.equals("call")) {
            return "Call the number: " + value;
        }
        if (command.equals("delete")) {
            return "The number " + value + " is deleted ";
        }
        if (command.equals("search")) {
            return "Searching the number: " + value;
        }
        if (command.equals("add")) {
            return "Add the number: " + value;
        } else return "Unknown command - " + command;
    }

    public String asLambda(String command, String value) {
        return operationCommand1.execute(command, value);
    }

    public String asMethodReference(String command, String value) {
        return operationCommand2.execute(command, value);
    }

    public String asAnonymousClass(String command, String value) {
        return operationCommand3.execute(command, value);
    }

    public String asObjectOfClass(String command, String value) {
        CommandImpl command1 = new CommandImpl();
        return command1.checkCommand(command, value);
    }


}
