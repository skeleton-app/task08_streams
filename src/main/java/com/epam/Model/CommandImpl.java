package com.epam.Model;

public class CommandImpl {
    Command newCommand = (command, value) -> {
        if (command.equals("call")) {
            return "Call the number: " + value;
        }
        if (command.equals("delete")) {
            return "The number " + value + " is deleted ";
        }
        if (command.equals("search")) {
            return "Searching the number: " + value;
        }
        if (command.equals("add")) {
            return "Add the number: " + value;
        } else return "Unknown command - " + command;
    };

    public String checkCommand(String command, String value) {
        return newCommand.execute(command, value);
    }

}
