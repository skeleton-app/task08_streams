package com.epam.Model;
@FunctionalInterface
public interface BiFunction {
    int function(int  a, int b, int c);
}
