package com.epam;

import com.epam.Model.APILists;
import com.epam.Model.BusinessLogic;
import org.apache.logging.log4j.LogManager;gi
import org.apache.logging.log4j.Logger;

public class Application {
    public static Logger logger = LogManager.getLogger(Application.class);

    public static void main(String[] args) {
        logger.info("Task1");
        BusinessLogic businessLogic = new BusinessLogic();
        logger.info("Max of three numbers 22, 24, 13 is " + businessLogic.getMax(22, 24, 13));
        logger.info("Average of three numbers 22, 24, 13 is " + businessLogic.getAverage(22, 24, 13));
        logger.info(" ");
        logger.info("Task2");
        logger.info("command as lambda ");
        logger.info(businessLogic.asLambda("delete", "0681845044"));
        logger.info("command as method reference ");
        logger.info(businessLogic.asLambda("call", "0681765111"));
        logger.info("command as anonymous class ");
        logger.info(businessLogic.asLambda("search", "0501764441"));
        logger.info("command as object of command class ");
        logger.info(businessLogic.asLambda("add", "0991766541"));
        logger.info(" ");
        logger.info("Task3");
        APILists apiLists = new APILists();
        logger.info("Get min");
        apiLists.getMin();
        logger.info("Get average");
        apiLists.getAvrg();
        logger.info("Get max");
        apiLists.getMax();
        logger.info("Get sum");
        apiLists.getSum();
        logger.info("Get amount of numbers > average");
        apiLists.getAmountOfNumbersBiggerAvrg();
        logger.info(" ");
        logger.info("Task4");
        apiLists.createApplication();
    }
}
